from Products.DexterityBibliography import _
from Products.DexterityBibliography.config import IDENTIFIER_TYPES, REFERENCE_TYPES
from Products.DexterityBibliography.helpers import format_author_name
from zope.schema.vocabulary import SimpleTerm, SimpleVocabulary
from Products.CMFCore.utils import getToolByName
from zope.schema.interfaces import IVocabularyFactory
from zope.interface import provider
from plone import api


def contenttypes():
    return SimpleVocabulary(
        [SimpleVocabulary.createTerm(t, _(t), t) for t in list(REFERENCE_TYPES)]
    )

def identifiertypes():
    return SimpleVocabulary([SimpleVocabulary.createTerm(t, _(t), t) for t in list(IDENTIFIER_TYPES)])

@provider(IVocabularyFactory)
def allauthors(context):
        catalog = api.portal.get_tool('portal_catalog')
        results = catalog.searchResults(portal_type=REFERENCE_TYPES)

        authors = []
        saved = []

        for brain in results:
            item = brain.getObject()
            for author in item.authors:
                if not author['lastname']:
                    continue

                save = format_author_name(author, "{L} {f}")
                if save in saved:
                    continue
                else:
                    saved.append(save)

                authors.append(author)

        authors.sort(key=lambda item: item["lastname"])
        return SimpleVocabulary([SimpleVocabulary.createTerm(format_author_name(author), format_author_name(author, "{L}, {F}"), format_author_name(author)) for author in authors])

