from bibtexparser.bibdatabase import BibDatabase
from bibtexparser.bparser import BibTexParser
from bibtexparser.customization import convert_to_unicode

import bibtexparser
#from Products.DexterityBibliography.config import PARSER_FORMATS, IDENTIFIER_TYPES
import re
from Products.DexterityBibliography import config


class BibTeXParser():
    def __init__(self):
        self.content_type_map = config.PARSER_FORMATS['bibtex']['content_type_map']
        self.content_type_map_vals = list(self.content_type_map.values())
        self.content_type_map_keys = list(self.content_type_map.keys())
        
    def parse_string(self, text_string):
        parser = BibTexParser()
        parser.ignore_nonstandard_types = False
        parser.customization = convert_to_unicode
        entries = bibtexparser.loads(text_string, parser=parser).entries
        for entry in entries:
            if (not 'author' in entry):
                if 'editor' in entry:
                    entry['author'] = entry['editor']
                else:
                    continue

            if entry['author'] != 'No names specified':
                authors = []
                author_strings = entry['author'].split(' and ')
                for author in author_strings:
                    if author == "":
                        continue
                    author_obj = {'homepage': None,
                                  'middlename': None}
                    parts = author.split(',')
                    if (len(parts) == 1):
                        author_obj['lastname'] = parts[0].strip()
                        author_obj['firstname'] = None
                    else:
                        author_obj['lastname'] = parts[0].strip()
                        if parts[1].find('.') != -1:
                            author_obj['firstname'] = parts[1].split('.')[0].strip()
                        else:
                            author_obj['firstname'] = parts[1].strip().split(' ')[0]


                    authors.append(author_obj)
                entry['authors'] = authors
            else:
                entry['authors'] = None

            identifiers = []
            for identifier_key in config.IDENTIFIER_TYPES:
                if identifier_key.lower() in entry:
                    identifier = entry[identifier_key.lower()]
                    if identifier.startswith(identifier_key.lower() + ':'):
                        length = len(identifier) + 1
                        identifier = identifier[length:]
                    identifiers.append({
                        'identifier_type': identifier_key,
                        'identifier_value': identifier
                    })
            entry['identifiers'] = identifiers
        return entries

    # WIP method name :)
    # This method assumes it only gets passed a list of ArticleReferences and
    # will need some restructuring when new content types get introduced.
    def reverse_parse(self, item_list):
        database = BibDatabase()
        database.entries = []
        for item in item_list:
            entry = {
                'title': item.title,
                'year': item.publication_year,
                "ENTRYTYPE": self.content_type_map_keys[self.content_type_map_vals.index(item.portal_type)],
                "ID": re.sub('[^A-Za-z0-9 ]+', '', item.title).lower().strip().replace(' ', '-')
            }
            if(hasattr(item, 'authors') and item.authors):
                author_string = ""
                for author in item.authors:
                    if(author != item.authors[0]):
                        author_string += ' and '
                    author_string += f'{author["lastname"]}, {author["firstname"]}'
                    middlename = author.get("middlename");
                    if (middlename is not None):
                        author_string += f' {middlename}'
                    author_string.strip()
                if hasattr(item, 'editor_flag'):
                    if item.editor_flag == True:
                        entry['editor'] = author_string
                    else:
                        entry['author'] = author_string
                else:
                    entry['author'] = author_string

            if(hasattr(item, 'identifiers') and item.identifiers):
                for identifier in item.identifiers:
                    if identifier['identifier_type']:
                        entry[identifier['identifier_type'].lower()] = str(identifier['identifier_value'])

            if(hasattr(item, 'journal') and item.journal):
                entry['journal'] = str(item.journal)

            if(hasattr(item, 'number') and item.number):
                entry['number'] = str(item.number)

            if(hasattr(item, 'pages') and item.pages):
                entry['pages'] = str(item.pages)
                
            if(hasattr(item, 'volume') and item.volume):
                entry['volume'] = str(item.volume)

            if(hasattr(item, 'abstract') and item.abstract):
                entry['abstract'] = str(item.abstract)

            if(hasattr(item, 'annotation') and item.annotation):
                entry['annote'] = str(item.annotation)

            if(hasattr(item, 'note') and item.note):
                entry['note'] = str(item.note)

            if(hasattr(item, 'series') and item.series):
                entry['series'] = str(item.series)

            if(hasattr(item, 'edition') and item.edition):
                entry['edition'] = str(item.edition)

            if(hasattr(item, 'publisher_address') and item.publisher_address):
                entry['address'] = str(item.publisher_address)

            if(hasattr(item, 'publisher') and item.publisher):
                entry['publisher'] = str(item.publisher)

            if(hasattr(item, 'how_published') and item.how_published):
                entry['howpublished'] = str(item.how_published)

            if(hasattr(item, 'book_title') and item.book_title):
                entry['booktitle'] = str(item.book_title)

            if(hasattr(item, 'organization') and item.organization):
                entry['organization'] = str(item.organization)

            if(hasattr(item, 'editor') and item.editor):
                entry['editor'] = str(item.editor)

            if(hasattr(item, 'chapter') and item.chapter):
                entry['chapter'] = str(item.chapter)

            if(hasattr(item, 'report_type') and item.report_type):
                entry['publication_type'] = str(item.report_type)

            if(hasattr(item, 'school') and item.school):
                entry['school'] = str(item.school)

            if(hasattr(item, 'institution') and item.institution):
                entry['institution'] = str(item.institution)

            database.entries.append(entry)

        if(len(database.entries) > 0):
            return bibtexparser.dumps(database)

        return None
