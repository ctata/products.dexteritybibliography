# ============================================================================
# DEXTERITY ROBOT TESTS
# ============================================================================
#
# Run this robot test stand-alone:
#
#  $ bin/test -s Products.DexterityBibliography -t test_bibliography_folder.robot --all
#
# Run this robot test with robot server (which is faster):
#
# 1) Start robot server:
#
# $ bin/robot-server --reload-path src Products.DexterityBibliography.testing.PRODUCTS_DEXTERITY_BIBLIOGRAPHY_ACCEPTANCE_TESTING
#
# 2) Run robot tests:
#
# $ bin/robot /src/Products/DexterityBibliography/tests/robot/test_bibliography_folder.robot
#
# See the http://docs.plone.org for further details (search for robot
# framework).
#
# ============================================================================

*** Settings *****************************************************************

Resource  plone/app/robotframework/selenium.robot
Resource  plone/app/robotframework/keywords.robot

Library  Remote  ${PLONE_URL}/RobotRemote

Test Setup  Open test browser
Test Teardown  Close all browsers


*** Test Cases ***************************************************************

Scenario: As a site administrator I can add a BibliographyFolder
  Given a logged-in site administrator
    and an add BibliographyFolder form
   When I type 'My BibliographyFolder' into the title field
    and I submit the form
   Then a BibliographyFolder with the title 'My BibliographyFolder' has been created

Scenario: As a site administrator I can view a BibliographyFolder
  Given a logged-in site administrator
    and a BibliographyFolder 'My BibliographyFolder'
   When I go to the BibliographyFolder view
   Then I can see the BibliographyFolder title 'My BibliographyFolder'


*** Keywords *****************************************************************

# --- Given ------------------------------------------------------------------

a logged-in site administrator
  Enable autologin as  Site Administrator

an add BibliographyFolder form
  Go To  ${PLONE_URL}/++add++BibliographyFolder

a BibliographyFolder 'My BibliographyFolder'
  Create content  type=BibliographyFolder  id=my-bibliography_folder  title=My BibliographyFolder

# --- WHEN -------------------------------------------------------------------

I type '${title}' into the title field
  Input Text  name=form.widgets.IBasic.title  ${title}

I submit the form
  Click Button  Save

I go to the BibliographyFolder view
  Go To  ${PLONE_URL}/my-bibliography_folder
  Wait until page contains  Site Map


# --- THEN -------------------------------------------------------------------

a BibliographyFolder with the title '${title}' has been created
  Wait until page contains  Site Map
  Page should contain  ${title}
  Page should contain  Item created

I can see the BibliographyFolder title '${title}'
  Wait until page contains  Site Map
  Page should contain  ${title}
