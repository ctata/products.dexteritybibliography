# ============================================================================
# DEXTERITY ROBOT TESTS
# ============================================================================
#
# Run this robot test stand-alone:
#
#  $ bin/test -s bibliography-folder -t test_pdf_folder.robot --all
#
# Run this robot test with robot server (which is faster):
#
# 1) Start robot server:
#
# $ bin/robot-server --reload-path src bibliography-folder.testing.BIBLIOGRAPHY-FOLDER_ACCEPTANCE_TESTING
#
# 2) Run robot tests:
#
# $ bin/robot /src/bibliography-folder/tests/robot/test_pdf_folder.robot
#
# See the http://docs.plone.org for further details (search for robot
# framework).
#
# ============================================================================

*** Settings *****************************************************************

Resource  plone/app/robotframework/selenium.robot
Resource  plone/app/robotframework/keywords.robot

Library  Remote  ${PLONE_URL}/RobotRemote

Test Setup  Open test browser
Test Teardown  Close all browsers


*** Test Cases ***************************************************************

Scenario: As a site administrator I can add a PdfFolder
  Given a logged-in site administrator
    and an add PdfFolder form
   When I type 'My PdfFolder' into the title field
    and I submit the form
   Then a PdfFolder with the title 'My PdfFolder' has been created

Scenario: As a site administrator I can view a PdfFolder
  Given a logged-in site administrator
    and a PdfFolder 'My PdfFolder'
   When I go to the PdfFolder view
   Then I can see the PdfFolder title 'My PdfFolder'


*** Keywords *****************************************************************

# --- Given ------------------------------------------------------------------

a logged-in site administrator
  Enable autologin as  Site Administrator

an add PdfFolder form
  Go To  ${PLONE_URL}/++add++PdfFolder

a PdfFolder 'My PdfFolder'
  Create content  type=PdfFolder  id=my-pdf_folder  title=My PdfFolder

# --- WHEN -------------------------------------------------------------------

I type '${title}' into the title field
  Input Text  name=form.widgets.IBasic.title  ${title}

I submit the form
  Click Button  Save

I go to the PdfFolder view
  Go To  ${PLONE_URL}/my-pdf_folder
  Wait until page contains  Site Map


# --- THEN -------------------------------------------------------------------

a PdfFolder with the title '${title}' has been created
  Wait until page contains  Site Map
  Page should contain  ${title}
  Page should contain  Item created

I can see the PdfFolder title '${title}'
  Wait until page contains  Site Map
  Page should contain  ${title}
