# -*- coding: utf-8 -*-
from plone import api
from plone.app.testing import setRoles, TEST_USER_ID
from plone.dexterity.interfaces import IDexterityFTI
from Products.DexterityBibliography.content.article_reference import (
    IArticleReference  # NOQA E501,,,,,,,,,,,,,,
)
from Products.DexterityBibliography.testing import (
    EXAMPLE_BIBLIOGRAPHY_FOLDER_INTEGRATION_TESTING  # noqa,,,,,,,,,,,,,,
)
from zope.component import createObject, queryUtility

import unittest


class ArticleReferenceIntegrationTest(unittest.TestCase):

    layer = EXAMPLE_BIBLIOGRAPHY_FOLDER_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        portal_types = self.portal.portal_types
        parent_id = portal_types.constructContent(
            'BibliographyFolder',
            self.portal,
            'parent_container',
            title='Parent container',
        )
        self.parent = self.portal[parent_id]

    def test_ct_article_reference_schema(self):
        fti = queryUtility(IDexterityFTI, name='ArticleReference')
        schema = fti.lookupSchema()
        self.assertEqual(IArticleReference, schema)

    def test_ct_article_reference_fti(self):
        fti = queryUtility(IDexterityFTI, name='ArticleReference')
        self.assertTrue(fti)

    def test_ct_article_reference_factory(self):
        fti = queryUtility(IDexterityFTI, name='ArticleReference')
        factory = fti.factory
        obj = createObject(factory)

        self.assertTrue(
            IArticleReference.providedBy(obj),
            u'IArticleReference not provided by {0}!'.format(
                obj,
            ),
        )

    def test_ct_article_reference_adding(self):
        setRoles(self.portal, TEST_USER_ID, ['Contributor'])
        obj = api.content.create(
            container=self.parent,
            type='ArticleReference',
            id='article_reference',
        )

        self.assertTrue(
            IArticleReference.providedBy(obj),
            u'IArticleReference not provided by {0}!'.format(
                obj.id,
            ),
        )

        parent = obj.__parent__
        self.assertIn('article_reference', parent.objectIds())

        # check that deleting the object works too
        api.content.delete(obj=obj)
        self.assertNotIn('article_reference', parent.objectIds())

    def test_ct_article_reference_globally_not_addable(self):
        setRoles(self.portal, TEST_USER_ID, ['Contributor'])
        fti = queryUtility(IDexterityFTI, name='ArticleReference')
        self.assertFalse(
            fti.global_allow,
            u'{0} is globally addable!'.format(fti.id)
        )
