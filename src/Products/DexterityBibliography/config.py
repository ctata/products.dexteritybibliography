from Products.CMFCore.permissions import AddPortalContent
from Products.DexterityBibliography import _
from Products.DexterityBibliography.parsers.bibtex import BibTeXParser


ADD_CONTENT_PERMISSION = AddPortalContent
PROJECTNAME = "DexterityBibliography"

USE_EXTERNAL_STORAGE = True

ENTRY_TYPES = 'bibtex_types'

FOLDER_TYPES = (
    'BibliographyFolder',
    'LargeBibliographyFolder',
)

PREPRINT_SERVERS = ('None:',
                    'arXiv:http://arxiv.org',
                    'CogPrints:http://cogprints.ecs.soton.ac.uk/',
                    )

REFERENCE_TYPES = (
    'ArticleReference',
    'BookletReference',
    'BookReference',
    'ConferenceReference',
    'InbookReference',
    'IncollectionReference',
    'InproceedingsReference',
    'ManualReference',
    'MastersthesisReference',
    'MiscReference',
    'PhdthesisReference',
    'PreprintReference',
    'ProceedingsReference',
    'TechreportReference',
    'UnpublishedReference',
    'WebpublishedReference',
    )

ZOPE_TEXTINDEXES = (
    'TextIndex',
    'ZCTextIndex',
    'TextIndexNG2',
    'TextIndexNG3',
)

IDENTIFIER_TYPES = (
    "DOI",
    "ISBN",
    "PMID",
    "ASIN",
    "PURL",
    "URN",
    "ISSN",
)

INPUT_ENCODINGS = (
    "UTF-8",
)

PARSER_FORMATS = {
    'bibtex': {
        'format': 'BibTeX',
        'description': _("A specific parser to process input in BibTeX-format."),
        'export_description': _("Export to native BibTeX format (with LaTeX escaping)"),
        'parser': BibTeXParser,
        'content_type_map': {
            "article": "ArticleReference",
            "book": "BookReference",
            "booklet" : "BookletReference",
            "conference" : "ConferenceReference",
            "inbook" : "InbookReference",
            "incollection" : "IncollectionReference",
            "manual" : "ManualReference",
            "mastersthesis": "MastersthesisReference",
            "misc": "MiscReference",
            "phdthesis": "PhdthesisReference",
            "proceedings": "ProceedingsReference",
            "inproceedings": "InproceedingsReference",
            "techreport": "TechreportReference",
            "unpublished": "UnpublishedReference",
            "preprint": "PreprintReference",
            "webpublished": "WebpublishedReference"
        }

    },
}



"""
if USE_EXTERNAL_STORAGE:
    try:
        import Products.ExternalStorage
    except ImportError:
        LOG(PROJECTNAME, PROBLEM, 'ExternalStorage N/A, falling back to AnnotationStorage')
        USE_EXTERNAL_STORAGE = False
"""
