from plone.dexterity.interfaces import IDexterityFTI
from Products.DexterityBibliography import _

"""
* Strucute for category
- title: ""
- description: ""
- reftypes: []
- tags: []
"""

class BibliographyFolderViewCategoryManager:
    filter_settings = {
        'filter_by_year': True,
        'filter_show': True,
        'filter_authors': ['Janosz, H.', 'Mustermann, M.'],
        'filter_years': [],
        'filter_default_author': "",
        'filter_default_year': "",
    }
    
    view_categories = [{
        "title": "Default",
        "description": "",
        "reftypes": [],
        "tags": [],
    }]

    def add_view_category(self, cat):
        if "title" in cat and "description" in cat and "reftypes" in cat and "tags" in cat:
            self.view_categories.append(cat)
            return True
        return False

    def get_view_categories(self):
        lis = []
        for i in range(len(self.view_categories)):
            cat = self.view_categories[i]
            cat["id"] = "i"
            lis.append(cat)
        return lis

