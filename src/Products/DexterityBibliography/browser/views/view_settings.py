from Products.DexterityBibliography.config import (
    REFERENCE_TYPES,
)
from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile


class ViewSettingsView(BrowserView):
    template = ViewPageTemplateFile("../templates/view_settings.pt")

    def __init__(self, context, request):
        self.context = context
        self.request = request
        
    def __call__(self):
        return self.template()

    def get_reference_types(self):
        return list(REFERENCE_TYPES)

        
