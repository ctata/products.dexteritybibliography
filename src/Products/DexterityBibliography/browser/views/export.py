from html.parser import HTMLParser
from plone import api
from plone.dexterity.utils import createContentInContainer
from plone.namedfile.file import NamedBlobFile
from plone.namedfile.utils import set_headers, stream_data
from Products.DexterityBibliography import _
from Products.DexterityBibliography.config import PARSER_FORMATS
from Products.DexterityBibliography.interfaces import (
    IProductsDexterityBibliographyUtility,
)
from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope.component import getUtility
from zope.publisher.interfaces import NotFound


class ExportView(BrowserView):
    template = ViewPageTemplateFile("../templates/export.pt")

    def __init__(self, context, request):
        self.gutil = getUtility(IProductsDexterityBibliographyUtility)
        self.context = context
        self.request = request

    def __call__(self):
        if not self.request.form.get('form.submitted'):
            return self.template()


        self.errors = {}
        parser_format = self.request.form.get('format', 'bibtex')
        eol_style = self.request.form.get('eol_style', 'nope')

        bib_objects = self.gutil.get_all_bib_child_objects(self.context, False)
        output = PARSER_FORMATS[parser_format]['parser']().reverse_parse(bib_objects)
        if output is None:
            print("Empty output from reverse parsing")
            output = ""

        if eol_style:
            output.replace('\n', '\r\n')

        blob_data = NamedBlobFile(output, filename='export.bib')

        # Sets Content-Type and Content-Length
        self.request.response.setHeader('Content-Type', 'application/octet-stream')
        set_headers(blob_data, self.request.response)

        # Set Content-Disposition
        self.request.response.setHeader(
            'Content-Disposition',
            'inline; filename={0}'.format('export.bib')
        )
        return stream_data(blob_data)
