from html.parser import HTMLParser
from plone import api
from plone.dexterity.utils import (
    addContentToContainer,
    createContent,
    createContentInContainer,
)
from Products.CMFCore.utils import getToolByName
from Products.DexterityBibliography import _
from Products.DexterityBibliography.config import PARSER_FORMATS
from Products.DexterityBibliography.interfaces import (
    IProductsDexterityBibliographyUtility,
)
from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from Products.statusmessages.interfaces import IStatusMessage
from zope.component import getUtility
from zope.lifecycleevent import modified

import logging
import sys


logger = logging.getLogger("Products.DexterityBibliography import")

# TODOs
# - [x] Fix the template regarding the format
# - [x] Implement the report building
# - [x] Add the logging
# - [x] Add DOI assignment to process_single_import()


class ImportView(BrowserView):
    template = ViewPageTemplateFile("../templates/import.pt")
    duplicate_count = 0

    def __init__(self, context, request):
        self.gutil = getUtility(IProductsDexterityBibliographyUtility)
        self.context = context
        self.request = request
        self.messages = IStatusMessage(self.request)

    def __call__(self):
        self.processed = False
        self.errors = {}
        start_time = self.context.ZopeTime().timeTime()
        if not self.request.form.get('form.submitted'):
            return self.template()

        # fetch value from request
        input_encoding = self.request.form.get('input_encoding', 'UTF-8')
        # span_of_search = self.request.form.get('span_of_search', None)

        parser_format = self.request.form.get('format', 'bibtex')

        #logger.info("Importing with input_encoding: " + str(input_encoding) + ", parser_format: " + str(parser_format))

        # Fetch the input/source from either the file or the text field
        filename = None
        source = self.request.form.get('up_text')

        if not source:
            logger.info("DEBUG: Going with uploaded file")
            upfile = self.request.form.get('file')
            filename = upfile and getattr(upfile, 'filename', None)
            if not filename:
                self.errors['file'] = _("You must import a file or enter a text.")
                return self.template()
            source = upfile.read().decode('utf-8')
            if not source or not type(source) == str:
                msg = "Could not read the file '%s'." % filename
                self.errors['file'] = msg
                return self.template()
        else:
            logger.info("DEBUG: Going with input from text field")

        # Skip DOS line breaks
        source = source.replace('\r', '')

        # Get the extracted entries from the parser as a list

        #logger.info("DEBUG: Attempting to use parser with key '" + parser_format + "'")
        parser = PARSER_FORMATS[parser_format]['parser']()
        entries = parser.parse_string(source)
        logger.info("Retrieved %s raw entries from input." % len(entries))

        # Start building the report
        mtool = getToolByName(self.context, 'portal_membership')
        member = mtool.getAuthenticatedMember()
        fullname = member.getProperty('fullname', None)
        if fullname:
            username = "%s (%s)" % (fullname, member.getId())
        else:
            username = member.getId()
        
        tmp_report = '[%s] Imported by %s' % (self.context.ZopeTime(),
                                              username)
        if filename is not None:
            tmp_report += ' from file %s' % filename
            tmp_report += ':\n\n'


        counter = 0
        processed_entries = 0
        import_errors = 0

        # print(str(self.context.get_duplicates_folder()))
        # print("REEEE: CREATED DUPLICATES FOLDA")

        #logger.info("Start import of %s raw entries" % len(entries))
        # Process the imported entries
        for entry in entries:
            counter += 1
            count = '#%05i:' % counter
            #logger.info(count + "Processing Entry")
            if isinstance(entry, str):
                msg = "Entry could not be parsed! %s" % entry
                upload = (msg, "error")
                logger.error(count + msg)
            elif entry.get('title'):
                #logger.info(count + "Successful normal processing of " + entry['title'])
                upload = self.process_single_import(entry, parser)
            else:
                formated = '; '.join(['%s=%s' % (key, entry[key])
                                      for key in sorted(entry.keys())
                                      if key == key.lower()])

                upload = ("Found entry without title: %s\n" % formated, "error")
                logger.error(count + upload[0])

            if upload[1] == "ok":
                processed_entries += 1
            else:
                import_errors += 1

            state, msg = upload[1].upper(), upload[0]
            tmp_report = "%s: %s\n" % (state, msg)
        msg = "Processed %i entries of which %i were duplicates. There were %i errors. "\
            "Import processed in %f seconds. See import report below." \
            % (processed_entries, self.duplicate_count, import_errors,
                self.context.ZopeTime().timeTime() - start_time)
        logger.info(msg)
        msgid = _(u"import_log_info", default=u"Processed ${entries} entries of which ${duplicates} were duplicates. There were ${errors} errors.", \
                  mapping={"entries": processed_entries, "duplicates": self.duplicate_count, "errors": import_errors})
        self.messages.add(msgid, type=u"info")

        #logger.info(tmp_report)
        return self.template()

    # Not sure if this method belongs here or just directly in the folder content type
    def process_single_import(self, entry, parser):
        import_status = "ok"
        new_context = 0
        try:
            # if (entry['ENTRYTYPE'] == 'article'):
            new_context = createContent(parser.content_type_map[entry['ENTRYTYPE']], title=entry["title"])
            new_context.assign_values_from_entry(entry)
            if self.context._get_duplicates(new_context):
                duplicates_folder = self.context.get_duplicates_folder()
                addContentToContainer(duplicates_folder, new_context)

                modified(duplicates_folder)
                self.duplicate_count += 1
            else:
                addContentToContainer(self.context, new_context)
        except:
            import_status = "Error type: %s. Error value: %s." \
                % (sys.exc_info()[0], sys.exc_info()[1])

        modified(self.context)
        return ("put_report_line", import_status)
