from html.parser import HTMLParser
from plone import api
from Products.DexterityBibliography import _
from Products.DexterityBibliography.config import REFERENCE_TYPES
from Products.DexterityBibliography.helpers import format_author_name
from Products.DexterityBibliography.interfaces import (
    IProductsDexterityBibliographyUtility,
)
from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope.component import getUtility
from zope.security import checkPermission
import html


# TODO:
# - [x] Make the get_authors() method check if there are more than 6 authors
# - [x] Implement/port get_formatted_identifiers
# - [ ] Add special formatting if the entry is an IncollectionReference, etc.
# - [ ] Add the filter class


class BibliographyFolder(BrowserView):
    index = ViewPageTemplateFile("../templates/folder.pt")
    filter_settings = {'filter_by_year': True}
    query = {}
    author_objects = []

    def debug_return_list(self):
        res = ""
        for i in self.context.portal_catalog(self.query):
            res += i.getURL() + "<br/>"

        return res

    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.gutil = getUtility(IProductsDexterityBibliographyUtility)
        self.filter_settings = self.context.filter_settings

    def __call__(self):
        self.initialize()
        return self.index()
        
    def initialize(self):
        self.query = {
            'path': '/'.join(self.context.getPhysicalPath()),
            'sort_on': 'publication_year'
        }

        if self.request.get('filter.author', None) is None:
            self.request.set('filter.author', self.filter_settings.get('filter_default_author', '').replace(',', ''))

        if self.request.get('filter.year', None) is None:
            self.request.set('filter.year', self.filter_settings.get('filter_default_year', ''))

        author = self.request.get('filter.author', '').strip().replace(',', ' ')
        author = filter(None, author.split(' '));
        author = list(map(str.strip, author))

        if author and author[0]:
           #query['SearchableText'] = author[0];
           self.query['author_items'] = ' '.join(author);

        year = self.request.get('filter.year', '').strip()
        if year:
            self.query['publication_year'] = self.gutil.publication_year_query_value(year);


    def run_query(self):
        results = self.context.portal_catalog(self.query, path={'query': self.query['path'], 'depth': 1})

        # from Products.CMFPlone import Batch
        # results = Batch(contents, 0, 1000, orphan=0)
        return self.sort_string_groups_alphabetically(self.group_bib_items_by_year(results))

    def sort_string_groups_alphabetically(self, groups):
        groups = dict(groups)
        year_entries = {}

        # Filter out numbers/years
        for key in list(groups.keys()):
            if key.isdigit():
                year_entries[key] = groups[key]
                del groups[key] 
        keys = list(groups.keys())
        keys.sort(reverse=True)
        sorted_string_entries = {i: groups[i] for i in keys}

        return {**year_entries, **sorted_string_entries}

    # In the original GEMI implementation this was used inside a utility class but im not
    # sure where else this might get used again and due to problems with getUtility I put it
    # here for now.
    def group_bib_items_by_year(self, list):
        """ Takes a list of Brains and returns a dictionary with keys being the years
        """
        res = {}
        for r in list:
            year = r.publication_year.lower().title()
            if not year in res:
                res[year] = [r]
            else: 
                res[year].append(r)

        return res

    def is_valid_year(self, year):
        year = year.lower().strip()
        if (year.isdigit()):
            return float(year) > 1900
        return year

    def can_add_content(self):
        return checkPermission('cmf.AddPortalContent', self.context)

class ViewListFormatter(BrowserView):
    item_template = ViewPageTemplateFile("../templates/folder_item.pt")

    def __init__(self, context, request):
        self.htmlparser = HTMLParser()
        self.context = context
        self.request = request
        self.gutil = getUtility(IProductsDexterityBibliographyUtility)

    def __call__(self, item=None):
        if(item is None):
            return None
        self.item = item
        self.item_object = self.item.getObject()
        return self.item_template()

    def is_correct_portal_type(self):
        # if (self.item.portal_type in REFERENCE_TYPES):
        #     return True
        # return False
        return True

    # TODO: Implement the specification for when there are more than 6 authors
    def get_authors(self):
        objects = self.gutil.get_author_objects(self.item_object)
        if (objects):

            _list = []

            if (len(objects) > 5):
                new_objects = objects[:6]
                new_objects.append(objects[len(objects) - 1]) 
                objects = new_objects
                
            for i, author in enumerate(objects):
                _a = author.get('lastname') 
                if author.get('firstname'):
                    _a += ', '
                    _a += author.get('firstname') + '.'
                if author.get('middlename'):
                    _a += ' ' + author.get('middlename') + '.'
                if author.get('eq_contrib'):
                    _a += '*'
                if author.get('print_eq_contrib'):
                    pass #_a += ' [*' + _('equally contributing') + ']'
                if len(_list) > 0:
                    _a = ', ' + _a

                _list.append(_a)

            if len(_list) > 1:
                lastIndex = len(_list) - 1
                if (len(objects) > 6):
                    _list[lastIndex] = ', ...' + _list[lastIndex].strip(',')
                else:
                    _list[lastIndex] = ' & ' + _list[lastIndex].strip(',')

            return ''.join(_list)
        return None

    def get_title(self):
        return self.item_object.title.strip(".") + "."

    def get_publication_year(self):
        return self.item_object.publication_year.strip()

    def get_journal(self):
        if not hasattr(self.item_object, "journal") or not self.item_object.journal:
            return False
        journal = str(self.item_object.journal)
        if self.format_volume():
            journal += ', '
        else:
            journal += '.'

        return journal

    def format_volume(self):
        item = self.item_object
        v = ''
        if (getattr(item, 'volume', None)):
            v += "<i>%s</i>" % item.volume
        if (getattr(item, 'number', None)):
            v += '('+ item.number +')'
        v = v.strip()
        if v and self.get_pages():
            v = v + ', '
        elif v and not self.get_pages():
            v = v + '.'

        return v

    def get_pages(self):
        if (getattr(self.item_object, 'pages', None)):
            return html.unescape(self.item_object.pages) + '.'
        return None

    def get_pdf_url(self):
        if self.item_object.pdf_url:
            return self.item_object.pdf_url.lower() if "/pdfs/" in self.item_object.pdf_url else self.item_object.pdf_url
            
        if not(hasattr(self.item_object.pdf_file, 'filename')):
            return False
        filename = self.item_object.pdf_file.filename.lower().replace(' ', '-')

        url = self.item_object.absolute_url() + "/@@download/pdf_file/" + filename
        #path = '/'.join(self.item_object.getPhysicalPath()[:-1])+ '/PDFs/'+ filename
        #url = path + '/@@display-file/file/'
        return url

    def get_formatted_identifiers(self):
        if (not hasattr(self.item_object, 'identifiers')):
            return None
        try:
            s = ' '.join([self.get_formatted_identifier(identifier) for identifier in self.item_object.identifiers]).strip(',').strip();
            if s:
                return s + '.';
        except:
            return None;

    def get_formatted_identifier(self, identifier):
        if (identifier['identifier_type'] == 'DOI'):
            idf = identifier['identifier_value'] if ("doi.org" in identifier['identifier_value']) else "https://dx.doi.org/" + identifier['identifier_value']
            return " %s," % idf
        else:
            return " %s: %s," % (identifier['identifier_type'], identifier['identifier_value'])

    def get_editors(self):
        if getattr(self.item, 'editor', None):
            return 'In ' + self.item_object.editor + ', '
        return None

    def get_book_title(self):
        if not getattr(self.item_object, 'book_title', None):
            return None

        s = ''
        if not self.get_editors:
            s += 'In '
        s += getattr(self.item_object, "book_title", None)
        if not s:
            return ""
        return s + '.'

    def get_edition(self):
        if (getattr(self.item_object, 'edition', None)):
            return ' (' + self.item_object.edition + ').'
        return None

    # Not a good name for the method but this is how it was called in the previous extension
    def inBook(self):
        """ chapter, pages. Publisher: Address """
        book = [];
        if (hasattr(self.item, 'chapter')):
            book.append(self.item_object.chapter)
        if (getattr(self.item, 'pages', None)):
            book.append('pp. ' + self.item_object.pages)
        book = filter(None, book)

        s = ''

        if book:
            s += '(' + ', '.join(book) + ').'
        if hasattr(self.item_object, 'publisher'):
            if hasattr(self.item_object, 'publisher_address'):
                s += ' %s: %s.' % (self.item_object.publisher_address, self.item_object.publisher)
            else:
                s += ' %s.' % self.item_object.publisher

        return s

    def get_publication_url(self):
        if self.item_object.publication_url:
            return self.item_object.publication_url
        else:
            return self.item.getURL()


class Filter(BrowserView):
    template = ViewPageTemplateFile("../templates/folder_filter.pt")

    def __init__(self, context, request):
        self.htmlparser = HTMLParser()
        self.context = context
        self.request = request
        self.filter_settings = {}
        self.gutil = getUtility(IProductsDexterityBibliographyUtility)

    def __call__(self, settings=None):
        if(settings is None):
            return None
        self.filter_settings = settings
        return self.template()

    def get_author_list(self):
        objects = self.gutil.get_all_bib_author_objects(self.context)
        res = []
        for item in objects:
            res.append(format_author_name(item, format="{L}, {F}"))

        return res

    
    def get_years_list(self):
        years = self.gutil.get_all_bib_years(self.context)

        if (len(years) > 1):
            years = [_('')] + years;
        return years
