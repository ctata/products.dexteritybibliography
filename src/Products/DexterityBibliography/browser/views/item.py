from html.parser import HTMLParser
from plone import api
from Products.DexterityBibliography import _
from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile


class BibliographyItem(BrowserView):
    def __init__(self, context, request):
        self.context = context
        self.request = request

    def get_authors(self):
        res = ""
        if (hasattr(self.context, 'authors')) and self.context.authors != None:
            for author in self.context.authors:
                if(author == self.context.authors[-1]):
                    res += ' and'
                elif (author != self.context.authors[0]):
                    res += ','

                if ('firstname' in author and author['firstname'] != None):
                    res += f' {author["firstname"]}'
                if ('middlename' in author and author['middlename'] != None):
                    res += f' {author["middlename"]}'
                res += f' {author["lastname"]}'

            return res.strip()
        return ""
