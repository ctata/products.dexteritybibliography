from plone.api.content import move
from plone.dexterity.utils import addContentToContainer
from Products.DexterityBibliography import _
from Products.DexterityBibliography.config import REFERENCE_TYPES
from Products.DexterityBibliography.interfaces import (
    IProductsDexterityBibliographyUtility,
)
from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from Products.statusmessages.interfaces import IStatusMessage
from zope.component import getUtility


class DuplicatesManagementView(BrowserView):
    template = ViewPageTemplateFile("../templates/duplicates.pt")

    def __init__(self, context, request):
        self.context = context
        self.request = request
        self.duplicates_criteria = self.context.get_duplicates_criteria()
        self.gutil = getUtility(IProductsDexterityBibliographyUtility)
        self.messages = IStatusMessage(self.request)

    def __call__(self):
        # CRITERIA UPDATING
        if self.request.form.get('form.submitted.criteria'):
            new_duplicates_criteria = {}
            criteria_map = self.context.get_available_criteria()
            for contenttype in list(criteria_map.keys()):
                for field in criteria_map[contenttype]:
                    if self.request.get(self.get_fields_checkbox_name(contenttype, field[0])):
                        if not contenttype in new_duplicates_criteria:
                            new_duplicates_criteria[contenttype] = []
                        new_duplicates_criteria[contenttype].append(field[0])

            self.context.set_duplicates_criteria(new_duplicates_criteria)
            self.messages.add(_("Criteria defined"), type=u"info")
            self.request.response.redirect(self.request["ACTUAL_URL"])

        # DUPLICATES HANDLING
        if self.request.form.get('form.submitted.manage'):
            duplicates_folder = self.context.get_duplicates_folder()

            for duplicate in self.get_duplicates_folder_content():
                choice = self.request.form.get(self.get_row_name(duplicate))
                # A check for 'delay' is not really needed.
                print("%s\t choosen for %s" % (choice, duplicate.title))
                if choice == 'find':
                    pass
                elif choice == 'skip':
                    duplicates_folder.manage_delObjects([duplicate.getId()])
                elif choice == 'replace':
                    ids = [x.getId() for x in self.context._get_duplicates(duplicate, span_of_search='local')]
                    if ids:
                        try:
                            self.context.manage_delObjects(ids)
                            move(duplicate, self.context, safe_id=True)
                        except:
                            pass

                elif choice == 'force':
                    move(duplicate, self.context, safe_id=True)


                self.messages.add(_("Action completed."), type=u"info")

            if not self.get_duplicates_folder_content():
                self.context.delete_duplicates_folder()

        # ENTRY RESCANNING
        if self.request.form.get('form.submitted.scan'):
            search_span = self.request.form.get('span_of_search')
            sort_on = self.request.form.get('sort_on')
            duplicates_count = 0

            query = {
                'portal_type': tuple(REFERENCE_TYPES)
            }
            if search_span == 'local':
                query['path'] = {'query': '/'.join(self.context.getPhysicalPath())}
            elif search_span == 'global':
                query['path'] = '/'
            else:
                raise ValueError("span of search for duplicates has an invalid value : %s" % search_span)

            cat = self.context.portal_catalog
            brains = cat(query)
            if (len(cat(brains)) < 1):
                return False

            objs = [brain.getObject() for brain in brains]
            objs.sort(key=lambda x: str(getattr(x, sort_on)) if hasattr(x, sort_on) else "")
            moved_objects = 0


            for obj in objs:
                print("* Currently checking %s : %s" % (str(obj.title), str(obj.getId())))
                if obj.aq_parent.portal_type == "DuplicatesFolder":
                    continue
                duplicates = self.context._get_duplicates(obj)
                if duplicates:
                    print("  - Found %d duplicates" % len(duplicates))
                    duplicates_count += len(duplicates)
                    for duplicate in duplicates:
                        move(duplicate, self.context.get_duplicates_folder(), safe_id=True)
                        moved_objects += 1
                        if duplicate in objs:
                            objs.remove(duplicate)
            print("  - Moved %s objects around." % moved_objects)

            self.messages.add(_("${count} new duplicate(s) found", mapping={'count': duplicates_count}), type=u"info")
            self.request.response.redirect(self.request["ACTUAL_URL"])


        return self.template()

    def get_duplicates_folder_content(self):
        """
        Returns all the duplicate objects stored in the duplicates folder, aka the
        already detected duplicates.
        """
        cat = self.context.portal_catalog
        brains = cat({'path': ('/'.join(self.context.getPhysicalPath())) + '/duplicates', 'sort_on': 'publication_year'})
        res = [brain.getObject() for brain in brains]
        return res

    def is_field_checked(self, contenttype, field):
        """
        Returns 'checked' if criteria field is stored in '_duplicates_criteria' in the
        criteria manager, ergo is select and supposed to be checked.
        If not it returns None.
        """
        if contenttype in self.duplicates_criteria and field in self.duplicates_criteria[contenttype]:
            return "checked"
        return None

    def get_fields_checkbox_name(self, contenttype, field):
        """
        Returns name used for the fields corresponding checkbox in the template. This
        function exists mostly for consistency purposes.
        """
        return contenttype + "_" + field

    # TODO: Proper implement this function
    def get_current_search_span(self):
        return 'global'

    def get_row_name(self, duplicate):
        return "row[%s]" % (duplicate.getId());
