
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"
	  xmlns:tal="http://xml.zope.org/namespaces/tal"
	  xmlns:metal="http://xml.zope.org/namespaces/metal"
	  xmlns:i18n="http://xml.zope.org/namespaces/i18n"
	  lang="en"
	  metal:use-macro="context/main_template/macros/master"
	  i18n:domain="Products.DexterityBibliography">

  <head>
	<metal:block fill-slot="style_slot">
	  <link rel="stylesheet" type="text/css"
			tal:define="navroot context/@@plone_portal_state/navigation_root_url"
			tal:attributes="href string:${navroot}/++resource++Products.DexterityBibliography/folder.css"
			/>
	</metal:block>
  </head>

  <body>
	<div metal:fill-slot="main">
	  <div tal:replace="structure provider:plone.abovecontenttitle" />
	  <h1 class="documentFirstHeading"><span tal:content="here/Title" />
		- <span i18n:translate="heading_manage_duplicate">Bibliographical Duplicates Management</span></h1>

	  <div tal:replace="structure provider:plone.belowcontenttitle" />
	  <form name="manage_duplicates"
			method="post"
			action="#"
			tal:attributes="action string:${context/absolute_url}/@@duplicatesmanagementform"
			tal:condition="view/get_duplicates_folder_content">
		<input type="hidden"
			   name="form.submitted.manage"
			   value="1">

		<fieldset>

		  <legend i18n:translate="duplicates_manage_existing">Bibliography Folder - Manage Existing Duplicates</legend>
		  <p class="formHelp"
			 i18n:translate="duplicates_find_matches_button">Members of your site may have added new bibliographical entries recently. To update the
			duplicate bibliographical entries' matching objects, use the "find matches" radio buttons. This
			is recommended before managing the listed duplicate bibliographical entries.</p>

		  <table class="table duplicates">
			<thead>
			  <tr>
				<th i18n:translate="bibliography_duplicateman_title" i18n:domain="plone" title="titles">Title</th>
				<th>
				  <span title="do nothing for now"
						i18n:attributes="title bibliography_delay_title"
						i18n:translate="bibliography_duplicateman_delay">
					delay</span>
				</th>
				<th>
				  <span title="remove items from the list of duplicates"
						i18n:translate="bibliography_skip"
						i18n:attributes="title bibliography_duplicateman_skip_title">
					drop/skip</span>
				</th>
				<th>
				  <span title="replace item in bibliography folder with item in duplicates manager"
						i18n:attributes="title bibliography_replace_title"
						i18n:translate="bibliography_replace">
					replace</span>
				</th>

				<th>
				  <span title="force items into bibliography folder without removing matching items"
						i18n:attributes="title bibliography_force_title"
						i18n:translate="bibliography_duplicateman_force">
					add/force</span>
				</th>
			  </tr>
			</thead>
			<tbody>
			  <tr tal:repeat="duplicate view/get_duplicates_folder_content">
				<tal:tag tal:define="row_name python: view.get_row_name(duplicate)">
					<td tal:content="duplicate/title"></td>
					<td style="vertical-align:top; text-align:center;">
						<input type="radio" name="" value="delay" tal:attributes="name row_name" checked="checked"/>
					</td>
					<td style="vertical-align:top; text-align:center;">
						<input type="radio" name="" value="skip" tal:attributes="name row_name" />
					</td>
					<td style="vertical-align:top; text-align:center;">
						<input type="radio" name="" value="replace" tal:attributes="name row_name" />
					</td>
					<td style="vertical-align:top; text-align:center;">
						<input type="radio" name="" value="force" tal:attributes="name row_name" />
					</td>
				</tal:tag>
			  </tr>

			</tbody>
		  </table>
		  <input class="context m-10 mb-20"
				 name="form.button.manage"
				 type="submit"
				 value="Submit changes"
				 i18n:attributes="value" />

	  </form>

	  <form name="scan"
			method="post"
			action="#"
			tal:attributes="action string:${context/absolute_url}/@@duplicatesmanagementform">
		<input type="hidden"
			   name="form.submitted.scan"
			   value="1">

		<legend i18n:translate="duplicates_bibfolderscan">
		  Bibliography Folder - (Re-)scan for duplicate bibliographical entries
		</legend>
		<p class="formHelp" i18n:translate="help_duplicates_bibfolderscan">
		  In some cases you may want to (re-)scan this bibliography folder for duplicate entries.
		</p>
		<div class="field">
		  <label i18n:translate="label_duplicates_bibfolderscan_order">Duplicates (re-)scanning Order</label>

		  <p class="formHelp" i18n:translate="help_duplicates_bibfolderscan_order">
			Specify the (re-)scanning order of duplicate entries in this bibliography folder.
			The order in that bibliographical entries are processed decides
			whether an entry is regarded as an original entry (first seen entry) or a duplicate.
		  </p>

		  <div style="float:left; padding-right:2em;">
			<select
			  width="100%"
			  tal:attributes="id string:sort_on;
							  name string:sort_on:text;">

			  <tal:sort_on tal:repeat="index python: view.gutil.get_sort_indexes()">
				<option selected="" value=""
						tal:attributes="value index;
										selected python: 'selected' if index == 'modified' else None;"
						i18n:translate="" i18n:domain="plone"><span tal:replace="index" /></option>
			  </tal:sort_on>
			</select>
		  </div>
		</div>

		<br/>
		<br/>
		<div class="field">
		  <label i18n:translate="label_duplicates_bibfolderscan_span">
			Duplicates (re-)scanning Search Span
		  </label>

		  <p class="formHelp" i18n:translate="select_span_search">
			Select the search span for duplicate bibliographical entries.
		  </p>

		  <div>
			<input type="radio"
				   name="span_of_search:text"
				   id="span_of_search_local"
				   checked="python: 'checked' if view.get_current_search_span() == 'local' else None"
				   value="local"
				   tal:attributes="checked python: view.context.get_duplicates_matching_policy() == 'local'" />

			<label for="span_of_search_local" i18n:translate="label_span_of_search_local">
			  Compare entries of this Bibliography Folder to items in this Bibliography Folder only (local search span)
			</label>

			<br />

			<input type="radio"
				   name="span_of_search:text"
				   id="span_of_search_global"
				   checked="python: 'checked' if view.get_current_search_span() == 'global' else None"
				   value="global"
				   tal:attributes="checked python: view.context.get_duplicates_matching_policy() == 'global'" />

			<label for="span_of_search_global" i18n:translate="label_span_of_search_global">
			  Compare entries of this Bibliography Folder to items on the whole site (global search span)
			</label>
		  </div>
		</div>

		<input class="context"
			   name="form.button.scan"
			   type="submit"
			   value="start duplicates scan"
			   i18n:attributes="value" />

	  </form>

	  <h2 i18n:translate="heading_manage_duplicates_criteria">Duplicates Criteria Management</h2>

	  <legend i18n:translate="legend_bibliographyfolder_duplicatescriteria">Bibliography Folder - Duplicates Criteria</legend>

	  <div class="field"
		   tal:define="form_action string:bibliographyfolder_managecriteria;">


		<div class="formHelp">
		  <p i18n:translate="help_duplicatescriteria_bibfolder_p1">
			The listing below shows the criteria
			that the duplicates engine of this bibliography folder checks on copy/cut+paste or
			import actions on bibliographical items. This bibliography folder's duplicate
			engine tries to find out if two different bibliographical entries refer
			to the same publication.
		  </p>
		  <p i18n:translate="help_duplicatescriteria_bibfolder_p2">
			To change a set of criteria for a bibliographical reference type, check the
			individual criteria of that type and click "Update All Duplicates Criteria"
			at the very bottom of this fieldset.
		  </p>
		</div>
	  </div>

	  <form name="check_duplicates"
			method="post"
			action="#"
			tal:attributes="action string:${context/absolute_url}/@@duplicatesmanagementform">
		<input type="hidden"
			   name="form.submitted.criteria"
			   value="1">

		<metal:block
		  tal:define="criteria_map python:view.context.get_available_criteria();
					  duplicates_criteria python:view.context.get_duplicates_criteria()"
		  tal:repeat="contenttype python: list(criteria_map.keys())">
		
			<div class="contenttypecriteria m-10">
				<h4 style="text-align:left; background: #efefef" tal:content="contenttype"></h4>
				<ul>
					<li tal:repeat="field python: criteria_map[contenttype]">
					<label>
						<input type="checkbox"  name=""  tal:attributes="name python: view.get_fields_checkbox_name(contenttype, field[0]); checked python:view.is_field_checked(contenttype, field[0])">
						<span tal:content="python: field[1]"></span>
					</label>
				  </li>
				</ul>
			</div>

		</metal:block>

		<input class="context"
			   name="form.button.save"
			   type="submit"
			   value="Update the criteria for duplicates management"
			   i18n:attributes="value" />

	  </form>

	</div>
  </body>
</html>
