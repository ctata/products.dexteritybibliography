from plone import api
from plone.dexterity.utils import createContentInContainer
from plone.namedfile import NamedFile


class BibliographyItemCreated:
    
    def __init__(self, item, event):
        self.item = item
        self.event = event

    def check_and_create_pdf_file(self):
        if (hasattr(self.item.pdf_file, "filename")):
            folders = api.content.find(depth = 1, context=self.item.__parent__, portal_type='PdfFolder')

            if (len(folders) >= 1):
                item = createContentInContainer(folders[0].getObject(), "PdfFile", title=self.item.pdf_file.filename)
                item.file = NamedFile(self.item.pdf_file.data, filename=self.item.pdf_file.filename)
