# -*- coding: utf-8 -*-
from plone.autoform import directives
from plone.autoform.interfaces import IFormFieldProvider
from plone.supermodel import model
from Products.DexterityBibliography import _
from Products.DexterityBibliography.behaviors.author import IAuthorSchema
from Products.DexterityBibliography.behaviors.fields import IIdentifierSchema
from Products.DexterityBibliography.content.base import BaseEntry, IBibliographyItem
from zope import schema
from zope.interface import implementer, provider


@provider(IFormFieldProvider)
class ITechreportReference(IBibliographyItem):
    """ Marker interface and Dexterity Python Schema for TechreportReference
    """

@implementer(ITechreportReference)
class TechreportReference(BaseEntry):
    """
    """

    def assign_values_from_entry(self, entry):
        self.abstract = entry.get("abstract")
        self.description = entry.get("abstract")
        self.authors = entry.get("authors")
        self.publication_year = entry.get("year")
        self.institution = entry.get("institution")
        self.report_type = entry.get("publication_type")
        self.publisher_address = entry.get("address")
        self.identifiers = entry.get("identifiers")
        self.publication_month = entry.get("month")
        self.note = entry.get("note")
        self.annote = entry.get("annote")
