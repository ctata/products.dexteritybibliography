# -*- coding: utf-8 -*-
# from plone.app.textfield import RichText
from plone.autoform import directives
from plone.autoform.interfaces import IFormFieldProvider
# from plone.namedfile import field as namedfile
from plone.supermodel import model
from Products.DexterityBibliography import _
from Products.DexterityBibliography.behaviors.author import IAuthorSchema
from Products.DexterityBibliography.behaviors.fields import IIdentifierSchema
from Products.DexterityBibliography.content.base import BaseEntry, IBibliographyItem
# from plone.supermodel.directives import fieldset
# from z3c.form.browser.radio import RadioFieldWidget
from zope import schema
from zope.interface import implementer, provider


@provider(IFormFieldProvider)
class IArticleReference(IBibliographyItem):
    """ Marker interface and Dexterity Python Schema for ArticleReference
    """

@implementer(IArticleReference)
class ArticleReference(BaseEntry):
    """
    """

    def assign_values_from_entry(self, entry):
        self.publication_year = entry.get('year')
        self.journal = entry.get('journal')
        self.abstract = entry.get('abstract')
        self.description = entry.get('abstract')
        self.number = entry.get('number')
        self.pages = entry.get('pages')
        self.volume = entry.get('volume')
        self.authors = entry.get('authors')
        self.identifiers = entry.get('identifiers')
        self.publication_month = entry.get('month')
        self.note = entry.get('note')
        self.annotation = entry.get('annote')
