# -*- coding: utf-8 -*-
from plone.autoform import directives
from plone.autoform.interfaces import IFormFieldProvider
from plone.dexterity.content import Container
from plone.namedfile.field import NamedFile
from plone.supermodel import model
from Products.DexterityBibliography import _
from zope.interface import implementer, Invalid, provider


def pdf_filetype_constraint(value):
    """ Check if the supplied filetype is a PDf.
    """
    if not (value.contentType == 'application/pdf'):
        raise Invalid("Please only upload Pdfs.")
    return True

@provider(IFormFieldProvider)
class IPdfFolder(model.Schema):
    """ Marker interface for PdfFolder
    """

@implementer(IPdfFolder)
class PdfFolder(Container):
    pass

@provider(IFormFieldProvider)
class IPdfFile(model.Schema):
    """ Marker interface for PdfFile
    """
    # directives.omitted('description')
    file = NamedFile(
        title=_("Please upload a PDF file."),
        required=False,
        constraint=pdf_filetype_constraint,
    )
