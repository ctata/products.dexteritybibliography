# -*- coding: utf-8 -*-
from plone.autoform import directives
from plone.autoform.interfaces import IFormFieldProvider
from plone.supermodel import model
from Products.DexterityBibliography import _
from Products.DexterityBibliography.behaviors.author import IAuthorSchema
from Products.DexterityBibliography.behaviors.fields import IIdentifierSchema
from Products.DexterityBibliography.content.base import BaseEntry, IBibliographyItem
from zope import schema
from zope.interface import implementer, provider


@provider(IFormFieldProvider)
class IIncollectionReference(IBibliographyItem):
    """ Marker interface and Dexterity Python Schema for IncollectionReference
    """

@implementer(IIncollectionReference)
class IncollectionReference(BaseEntry):
    """
    """

    def assign_values_from_entry(self, entry):
        self.publication_year = entry.get('year')
        self.publication_month = entry.get('month')
        self.abstract = entry.get('abstract')
        self.description = entry.get('abstract')
        self.authors = entry.get('authors')
        self.book_title = entry.get('booktitle')
        self.editor = entry.get('editor')
        self.publisher = entry.get('publisher')
        self.publisher_address = entry.get('address')
        self.edition = entry.get('edition')
        self.volume = entry.get('volume')
        self.series = entry.get('series')
        self.chapter = entry.get('chapter')
        self.pages = entry.get('pages')
        self.identifiers = entry.get('identifiers')
        self.report_type = entry.get('publication_type')
        self.note = entry.get('note')
        self.annotation = entry.get('annote')
