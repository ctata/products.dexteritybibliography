# -*- coding: utf-8 -*-
from plone.autoform import directives
from plone.autoform.interfaces import IFormFieldProvider
from plone.supermodel import model
from Products.DexterityBibliography import _
from Products.DexterityBibliography.behaviors.author import IAuthorSchema
from Products.DexterityBibliography.behaviors.fields import IIdentifierSchema
from Products.DexterityBibliography.content.base import BaseEntry, IBibliographyItem
from zope import schema
from zope.interface import implementer, provider


@provider(IFormFieldProvider)
class IBookletReference(IBibliographyItem):
    """ Marker interface and Dexterity Python Schema for BookletReference
    """

@implementer(IBookletReference)
class BookletReference(BaseEntry):
    """
    """

    def assign_values_from_entry(self, entry):
        self.publication_year = entry.get('year')
        self.publication_month = entry.get('month')
        self.abstract = entry.get('abstract')
        self.description = entry.get('abstract')
        self.authors = entry.get('authors')
        self.identifiers = entry.get('identifiers')
        self.note = entry.get('note')
        self.annotation = entry.get('annote')
        self.how_published = entry.get('howpublished')
