# -*- coding: utf-8 -*-
from plone.autoform.interfaces import IFormFieldProvider
from plone.dexterity.content import Container
from plone.supermodel import model
from Products.DexterityBibliography import _
from zope.interface import implementer, provider


@provider(IFormFieldProvider)
class IDuplicatesFolder(model.Schema):
    """ Marker interface for DuplicatesFolder
    """

@implementer(IDuplicatesFolder)
class DuplicatesFolder(Container):
    pass
