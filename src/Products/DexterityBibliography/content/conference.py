# -*- coding: utf-8 -*-
from plone.autoform import directives
from plone.autoform.interfaces import IFormFieldProvider
from plone.supermodel import model
from Products.DexterityBibliography import _
from Products.DexterityBibliography.behaviors.author import IAuthorSchema
from Products.DexterityBibliography.behaviors.fields import IIdentifierSchema
from Products.DexterityBibliography.content.base import BaseEntry, IBibliographyItem
from zope import schema
from zope.interface import implementer, provider


@provider(IFormFieldProvider)
class IConferenceReference(IBibliographyItem):
    """ Marker interface and Dexterity Python Schema for ConferenceReference
    """

@implementer(IConferenceReference)
class ConferenceReference(BaseEntry):
    """
    """

    def assign_values_from_entry(self, entry):
        self.publication_year = entry.get('year')
        self.publication_month = entry.get('month')
        self.abstract = entry.get('abstract')
        self.description = entry.get('abstract')
        self.number = entry.get('number')
        self.volume = entry.get('volume')
        self.authors = entry.get('authors')
        self.identifiers = entry.get('identifiers')
        self.publisher = entry.get('publisher')
        self.publisher_address = entry.get('address')
        self.book_title = entry.get('booktitle')
        self.organization = entry.get('organization')
        self.note = entry.get('note')
        self.annotation = entry.get('annote')
        self.series = entry.get('series')
        self.pages = entry.get('pages')
