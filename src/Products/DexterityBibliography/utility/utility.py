from DateTime import DateTime
from Products.CMFCore.utils import getToolByName
# from bibliograph.core.utils import _decode
from Products.DexterityBibliography import _
from Products.DexterityBibliography.config import (
    INPUT_ENCODINGS,
    PARSER_FORMATS,
    REFERENCE_TYPES,
)
from Products.DexterityBibliography.interfaces import (
    IProductsDexterityBibliographyUtility,
)
from zope.interface import implementer, implements


@implementer(IProductsDexterityBibliographyUtility)
class ProductsDexterityBibliographyUtility(object):
    def group_bib_items_by_year(self, pub_list):
        """ Takes a list of Brains and returns a dictionary with keys being the years
        """
        res = {}
        for r in pub_list:
            year = r.publication_year.lower().title();
            if not res.has_key(year):
                res[year] = [r]
            else: 
                res[year].append(r)
        return res

    def get_author_objects(self, item):
        if not (hasattr(item, 'authors')) or not getattr(item, 'authors'):
            return []
        authors = item.authors
        objects = []
        _list = []
        for a in authors:
            if not a.get('lastname'):
                continue

            obj = {'lastname': a.get('lastname')}
            if a.get('firstname'):
                fn = a.get('firstname').strip('*').split('-')
                obj['firstname'] = '.-'.join([name[0] for name in fn])
            if a.get('middlename'):
                obj['middlename'] = a.get('middlename')[0]

            objects.append(obj)
        return objects

    # Currently these few methods using queries and the catalog are finding viable child
    # objects by looking for the 'publication_year' field thats shared across all
    # entries, im not sure if this is really viable though.
    def get_all_bib_child_objects(self, folder, only_published=False):
        """ Returns all Bibliographical child entries from a passed folder. """
        catalog = getToolByName(self, 'portal_catalog')
        query = {
            'path': '/'.join(folder.getPhysicalPath()),
            'portal_type': tuple(REFERENCE_TYPES)
        }
        if only_published:
            brains = catalog(query, review_state="published")
        else:
            brains = catalog(query)
        lis = []
        for brain in brains:
            lis.append(brain.getObject())

        return lis

    def get_all_bib_author_objects(self, folder):
        """Get all author objects from an entire folder."""
        catalog = getToolByName(self, 'portal_catalog')
        query = {
            'path': '/'.join(folder.getPhysicalPath()),
            'sort_on': 'publication_year'
        }
        brains = catalog(query)
        authors_save = []

        for brain in brains:
            authors = self.get_author_objects(brain.getObject())
            authors_save.extend(author for author in authors if author not in authors_save)

        authors_save.sort(key=lambda item: item["lastname"])
        return authors_save


    def get_all_bib_years(self, folder):
        catalog = getToolByName(self, 'portal_catalog')
        query = {
            'path': '/'.join(folder.getPhysicalPath()),
            'sort_on': 'publication_year'
        }
        brains = catalog(query)
        years_save = []

        for brain in brains:
            year = brain.getObject().publication_year.lower().title()
            if year and not year in years_save:
                years_save.append(year)

        years_save.sort();
        return years_save;

    def publication_year_query_value(self, year):
        year = year.lower().strip();

        if year == 'in press':
            return {'query': ('in press', 'In Press', 'In press'), 'operator': 'or'}
        elif year == 'accepted':
            return {'query': ('accepted', 'Accepted'), 'operator': 'or'}

        return year;

    def get_input_encodings(self):
        return list(INPUT_ENCODINGS)

    def get_parser_formats_as_list(self):
        lis = []
        for key in PARSER_FORMATS:
            item = PARSER_FORMATS[key]
            item['key'] = key
            lis.append(item)
        return lis

    def get_sort_indexes(self, start_with_empty_element=False):
        """Returns a list of all sortable index ids from the catalog"""
        catalog = getToolByName(self, 'portal_catalog')
        indexes = catalog.indexes()
        sort_indexes = [i for i in indexes \
                            if catalog.Indexes[i].meta_type in ('FieldIndex', 'DateIndex', 'DateRangeIndex', 'KeywordIndex') ]
        sort_indexes.sort()
        if start_with_empty_element:
            return [''] + sort_indexes
        else:
            return sort_indexes

    def sort_by_authors(self, brain):
        authors = brain.getObject().author_items()
        if len(authors):
            return authors[0]
        else:
            return ""
