from collective.z3cform.datagridfield import DataGridFieldFactory, DictRow
from plone.autoform import directives
from plone.autoform.interfaces import IFormFieldProvider
from plone.dexterity.interfaces import IDexterityContent
from plone.supermodel import model
from Products.DexterityBibliography import _
from z3c.form.browser.radio import RadioFieldWidget
from zope import schema
from zope.interface import provider


@provider(IFormFieldProvider)
class IAuthor(model.Schema):
    """
    Interface class defining the properties of an author
    """

    firstname = schema.TextLine(
        title='First Name',
        required=False,
        )

    lastname = schema.TextLine(
        title='Last Name',
        required=False,
    )

    middlename = schema.TextLine(
        title='Middle Name',
        required=False,
    )

    homepage = schema.TextLine(
        title='Home Page',
        required=False,
    )


@provider(IFormFieldProvider)
class IAuthorSchema(model.Schema):

    """
    Schema for defining the authors attribute for article types
    """

    authors = schema.List(
        title=_(u'Authors'),
        value_type=DictRow(title=_(u'Table'), schema=IAuthor),
        default=[],
        required=True,
    )
    directives.widget('authors', DataGridFieldFactory)
