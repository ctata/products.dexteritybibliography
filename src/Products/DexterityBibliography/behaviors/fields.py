from collective.z3cform.datagridfield import DataGridFieldFactory, DictRow
from plone.autoform import directives
from plone.autoform.interfaces import IFormFieldProvider
from plone.dexterity.interfaces import IDexterityContent
from plone.supermodel import model
from Products.DexterityBibliography import _
from Products.DexterityBibliography.vocabularies.vocabulary import identifiertypes
from z3c.form.browser.radio import RadioFieldWidget
from zope import schema
from zope.interface import provider
from zope.schema.vocabulary import SimpleTerm, SimpleVocabulary


@provider(IFormFieldProvider)
class IJournalField(model.Schema):

    journal = schema.TextLine(
        title=_(u'Journal'),
        description=_("A journal name (should be the full journal name)."),
        required=False,
    )

@provider(IFormFieldProvider)
class IVolumeField(model.Schema):
    
    volume = schema.TextLine(
        title=_(u'Volume'),
        description=_("The volume of a journal, multivolume book work etc. In most cases it should suffice to use a simple number as the volume identifier."),
        required=False,
    )

@provider(IFormFieldProvider)
class INumberField(model.Schema):

    number = schema.TextLine(
        title=_("Number"),
        description=_("The number of a journal, proceedings, technical report etc. Issues of journals, proceedings etc. are usually identified by volume and number; however, issues of the same volume will often be numbered preceedingly which often makes the specification of a number optional. With technical reports, the issuing organization usually also gives it a number."),
        required=False,
    )

@provider(IFormFieldProvider)
class IPagesField(model.Schema):

    pages = schema.TextLine(
        title=_("Pages"),
        description=_("A page number or range of numbers such as '42-111'; you may also have several of these, separating them with commas: '7,41,73-97'."),
        required=False,
    )

@provider(IFormFieldProvider)
class IIdentifier(model.Schema):
    """
    Interface class defining the properties of an identifier entry
    """
    identifier_type = schema.Choice(
        title=_("Identifier"),
        required=False,
        vocabulary=identifiertypes(),
    )

    identifier_value = schema.TextLine(
        title=_("Value"),
        required=False,
    )

@provider(IFormFieldProvider)
class IIdentifierSchema(model.Schema):
    identifiers= schema.List(
        title=_("Identifiers"),
        value_type=DictRow(title=_("Table"), schema=IIdentifier),
        default=[],
        required=False,
    )
    directives.widget("identifiers", DataGridFieldFactory)

@provider(IFormFieldProvider)
class IPublicationMonthField(model.Schema):
    publication_month = schema.TextLine(
        title=_("Publication Month"),
        description=_("Month of publication (or writing, if not published)."),
        required=False,
    )

@provider(IFormFieldProvider)
class INoteField(model.Schema):
    note = schema.Text(
        title=_("Note"),
        description=_("Any additional information that can help the reader. The first word should be capitalized."),
        required=False,
    )

@provider(IFormFieldProvider)
class IAnnotationField(model.Schema):
    annotation = schema.Text(
        title=_(" Personal annotation "),
        description=_("Any annotation that you do not wish to appear in rendered (BibTeX) bibliographies."),
        required=False,
    )

@provider(IFormFieldProvider)
class IEditorFlagField(model.Schema):
    editor_flag = schema.Bool(
        title=_("Editor Flag"),
        description=_("Check here if the author(s) specified above are actually the editor(s) of the referenced resource."),
        required=False,
    )


@provider(IFormFieldProvider)
class IPublisherField(model.Schema):
    publisher = schema.TextLine(
        title=_("Publisher"),
        description=_("The publisher's name."),
        required=True,
    )

@provider(IFormFieldProvider)
class IPublisherAddressField(model.Schema):
    publisher_address = schema.TextLine(
        title=_("Publisher Address"),
        description=_("Publisher's address. For major publishing houses, just the city is given. For small publishers, you can help the reader by giving the complete address."),
        required=False,
    )

@provider(IFormFieldProvider)
class IEditionField(model.Schema):
    edition = schema.TextLine(
        title=_("Edition"),
        description=_("The edition of a book."),
        required=False,
    )

@provider(IFormFieldProvider)
class ISeriesField(model.Schema):
    series = schema.TextLine(
        title=_("Series"),
        description=_("The name of a series or set of books. When citing an entire book, the 'title' field gives its title and this optional 'series' field gives the name of a series in which the book is published."),
        required=False,
    )


@provider(IFormFieldProvider)
class IHowPublishedField(model.Schema):
    how_published = schema.TextLine(
        title=_("How published"),
        description=_("For publications without a publisher. e.g., an 'Institute Report'."),
        required=False,
    )

@provider(IFormFieldProvider)
class IBookTitleField(model.Schema):
    book_title = schema.TextLine(
        title=_("Book Title"),
        description=_("Title of the book, collection or proceedings, that the cited resource is part of."),
        required=True,
    )

@provider(IFormFieldProvider)
class IOrganizationField(model.Schema):
    organization = schema.TextLine(
        title=_("Organization"),
        description=_("The organization sponsoring a conference, issuing a technical report etc."),
        required=False,
    )


@provider(IFormFieldProvider)
class IEditorField(model.Schema):
    editor = schema.TextLine(
        title=_("Editor"),
        description=_("Name(s) of editor(s). Opposed to the 'author' field, the 'editor' field should contain the editor of the book, collection or proceeding that the reference appears in."),
        required=True,
    )


@provider(IFormFieldProvider)
class IChapterField(model.Schema):
    chapter = schema.TextLine(
        title=_("Chapter"),
        description=_("A chapter number."),
        required=False,
    )


@provider(IFormFieldProvider)
class IReportTypeField(model.Schema):
    report_type = schema.TextLine(
        title=_("Type"),
        description=_("The type of a technical report, PhD thesis etc. - for example, 'Research Note' or 'Doktorarbeit'."),
        required=False,
    )


@provider(IFormFieldProvider)
class ISchoolField(model.Schema):
    school = schema.TextLine(
        title=_("School"),
        description=_("The name of the school (college, university etc.) where a thesis was written."),
        required=False,
    )


@provider(IFormFieldProvider)
class IInstitutionField(model.Schema):
    school = schema.TextLine(
        title=_("Institution"),
        description=_("The institution that published the work."),
        required=True,
    )

@provider(IFormFieldProvider)
class IPreprintServerField(model.Schema):
    preprint_server = schema.TextLine(
        title=_("Preprint Server"),
        description=_("If the preprint is available from one of the following preprint servers, you can indicate that here. Contact the site's admin if you want a server to be added to the list."),
        required=False
    )
