from Products.DexterityBibliography import _


def format_author_name(author, format="{L} {f}"):

    firstname = author.get('firstname')
    lastname = author.get('lastname')
    middlename = author.get('middlename')

    firstname = firstname if firstname else None
    lastname = lastname if lastname else None
    middlename = middlename if middlename else None

    str = format.format(
        L=(lastname if lastname is not None or '' else ''),
        l=(lastname[0] if lastname is not None or '' else ''),
        F=(firstname if firstname is not None or '' else ''),
        f=(firstname[0] if firstname is not None or '' else ''),
        M=(middlename if middlename is not None or '' else ''),
        m=(middlename[0] if middlename is not None or '' else ''),
    )

    return str.strip()
