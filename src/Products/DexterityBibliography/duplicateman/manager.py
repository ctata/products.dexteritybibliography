from plone.dexterity.utils import createContentInContainer
from Products.CMFPlone.utils import _createObjectByType


class BibliographyFolderDuplicatesManager:
    _duplicates = []

    _duplicates_folder_name = 'duplicates'

    _duplicates_by_type = {}

    def __init__(self):
        self._duplicates_by_type = {}

    # TODO: This needs to get restructured a bit, since the 'duplicates_matching_policy' field
    #       will have different values based on the set language. It should probably use a translatable
    #       vocabulary instead. This is just a rudimentary setup to get some stuff working.
    def get_duplicates_matching_policy(self):
        if self.duplicates_matching_policy == "This Plone site":
            return 'global'
        return 'local'

    def get_duplicates_folder(self, id='duplicates'):
        """
        Returns the folder duplicates get stored in, creates one if it does
        note exist already.
        """
        cat = self.portal_catalog
        query = {
            'path': '/'.join(self.getPhysicalPath()),
            'id': id
        }

        res = cat(query)
        if len(res) > 0:
            return res[0].getObject()
            
        return _createObjectByType("DuplicatesFolder", self, id)

    def delete_duplicates_folder(self):
        """
        Checks if the duplicates folder exists and if so deletes it.
        """
        if "duplicates" in self:
            self.manage_delObjects([self['duplicates'].getId()])

    
    # TODOs:
    # - [ ] Maybe rename to 'get_duplicates()'
    # - [ ] Add span_of_search checking
    # - [ ] Need to check if attribute even exists
    # - [ ] Pass already imported objects to parameters
    def _get_duplicates(self, item, span_of_search='local'):
        """
        Checks if the passed item already exists in the specified scope. 
        The scope can either be 'local', checking inside the local folder
        instance, or 'global', checking the entire folder.
        
        Returns the already existing duplicate objects if they exist, otherwise false.
        """
        report = "\n----------- Checking for duplicates of %s (%s).\n" % (item.title, item.getId())
        objs = self._get_search_objects(item.portal_type, span_of_search)
        if (len(objs) < 1):
            return False
        
        duplicates = []
        if item in objs:
            objs.remove(item)
        report += "- [+] %d possible duplicates have been found.\n" % len(objs)

        defined_criteria = self.get_duplicates_criteria()
        for obj in objs:
            report += "- Checking with already existing object %s.\n" % obj.getId()
            if obj.aq_parent.portal_type == "DuplicatesFolder":
                report += "  - ! NOT A DUPLICATE. REASON: Resides in duplicates folder.\n"
                continue

            is_duplicate = True
            duplicate_criteria = []
            if item.portal_type in defined_criteria:
                for criteria in defined_criteria[item.portal_type]:
                    if getattr(item, criteria) != getattr(obj, criteria):
                        if is_duplicate:
                            report += "  - ! NOT A DUPLICATE. Reason:\n"
                        report += "    - Different values in %s field.\n" % str(criteria)
                        report += "      Orig: %s\n" % str(getattr(obj, criteria))
                        report += "      New:  %s\n" % str(getattr(item, criteria))
                        is_duplicate = False
            else:
                report += "  - NOT A DUPLICATE. Reason: No duplicates criteria for this specific portal type was set.\n"
                continue

            if is_duplicate:
                report += "  - ! FLAGGED AS DUPLICATE..\n"
                duplicates.append(obj)


        #print("Found following duplicates %s" % str(duplicates))
        #print(report)
        return duplicates


    def _get_search_objects(self, portal_type, span_of_search):
        cache_key = "{type}_{span}".format(type=portal_type, span=span_of_search)

        if not cache_key in self._duplicates_by_type:
            cat = self.portal_catalog
            search_depth = 1

            query = {
                'portal_type': {'query': portal_type}
            }
            if span_of_search == 'local':
                query['path'] = {'query': '/'.join(self.getPhysicalPath()), 'depth': 1}
            elif span_of_search == 'global':
                query['path'] = '/'
                # Setting depth to 2 here will lead to the catalog also checking duplicates folders
                # which is not good.
            else:
                raise ValueError("span of search for duplicates has an  invalid value : %s" % span_of_search)

            
            print('Searching with query %s' % str(query))
            # Collect all objects with the same portal_type
            brains = self.portal_catalog(query)
            print('Objects found ' + str(len(brains)))
            if (len(brains) < 1):
                return []

            self._duplicates_by_type[cache_key] = [brain.getObject() for brain in brains]
        else:
            print ("using cache key " + cache_key)
        
        return self._duplicates_by_type[cache_key]
