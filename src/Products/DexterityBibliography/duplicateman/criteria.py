from plone.behavior.registration import (
    BehaviorRegistrationNotFound,
    lookup_behavior_registration,
)
from plone.dexterity.interfaces import IDexterityFTI
from Products.DexterityBibliography import _
from Products.DexterityBibliography.config import REFERENCE_TYPES
from zope.component import getUtility
from zope.interface.interfaces import ComponentLookupError
from zope.schema import getFieldsInOrder


class BibliographyFolderDuplicatesCriteriaManager:

    # Contains criteria that should not be available to choose to check for
    _ignored_criteria = ['id', 'note', 'annotation', 'pdf_url', 'pdf_reference', 'pdf_file', 'explain_links', 'abstract', 'publication_url', 'editor_flag', 'book_title']


    # Contains the default duplicates criteria
    _default_criteria = [
        'authors',
        'portal_type',
        'publication_year',
        'title',
    ]

    # Contains the possible criteria from every bibliograhpy type
    _available_criteria = {}

    # Contains criteria to be checked for duplication while importing for each bibliograhpy type
    _duplicates_criteria = {}

    # List fields from behaviors that should not be inclided in the duplicate management criteria
    excludes = []

    def get_duplicates_criteria(self):
        return self._duplicates_criteria

    def set_duplicates_criteria(self, duplicates_map):
        self._duplicates_criteria = duplicates_map
    
    def set_default_criteria(self):
        default = {}
        for type in REFERENCE_TYPES:
            default[type] = self._default_criteria

        return self.set_duplicates_criteria(default)


    def get_available_criteria(self):
        if len(self._available_criteria) == 0:
            self.init_available_criteria()
            
        return self._available_criteria

    # TODO: Sort criteria by their i18n title
    def sort_criteria_by_title(self):
        pass

    def get_all_fields_from_schema(self, schema):
        """
        Returns a list with all field names as Strings from the passed schema.
        Also looks through all provided behavior.
        """
        fields = getFieldsInOrder(schema)
        return [(field[0], field[1].title) for field in fields if field[0] not in self._ignored_criteria]

    def init_available_criteria(self):

        for type in REFERENCE_TYPES:
            try:
                fields = [('title', _('Title'))];
                fti = getUtility(IDexterityFTI, name=type)
                behaviors = fti.behaviors
                for bname in behaviors:
                    if 'Products.DexterityBibliography.behaviors' in bname:
                        behavior = lookup_behavior_registration(name=bname)
                        fields += self.get_all_fields_from_schema(behavior.interface)

                self._available_criteria[type] = fields
            except (BehaviorRegistrationNotFound, ComponentLookupError):
                pass
