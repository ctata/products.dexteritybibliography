from Products.DexterityBibliography.listeners.folder import BibliographyFolderCreated
from Products.DexterityBibliography.listeners.item import BibliographyItemCreated


def bibliography_folder_created(folder, event):
    listener = BibliographyFolderCreated(folder, event)
    listener.create_pdf_folder()
    listener.check_duplicates()


def bibliography_item_created(item, event):
    listener = BibliographyItemCreated(item, event)
    listener.check_and_create_pdf_file()
